﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeDetector : MonoBehaviour {
    public Renderer plane;
    private Vector3 accel;
    public float magni;
    public float interval;
    private float timelastshake;
    
    void Update () {
        accel = Input.acceleration;
        if (accel.sqrMagnitude >= magni && Time.unscaledTime >= timelastshake + interval) {
            plane.material.SetColor("_Color", Random.ColorHSV());
            timelastshake = Time.unscaledTime;
        }
    }
}
