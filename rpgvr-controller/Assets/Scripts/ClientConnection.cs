﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

using UnityEngine;
using UnityEngine.Networking;


//public class RegisterHostMessage : MessageBase
//{
//    public string gameName;
//    public string comment;
//    public bool passwordProtected;
//}

public class ClientConnection : MonoBehaviour {
    //Necessary to discover screen IP
    public UdpClient Server;
    public byte[] ResponseData;
    IPEndPoint ClientEP;
    byte[] ClientRequestData;
    string ClientRequest;
    string ipv4;
    int port = 8888;

    
//    public void DisconnectFromServer() {
//        if (GlobalSettings.Client.isConnected) {
//            RegisterHostMessage msg = new RegisterHostMessage();
//            msg.comment = "shutdown";
//            msg.passwordProtected = false;
//            GlobalSettings.Client.Send(888, msg);
//
//            GlobalSettings.Client.Disconnect();
//        }
//    }

    public void ConnectToServer() {
        Server = new UdpClient(port);
        Server.Client.SendTimeout = 5000;
        Server.Client.ReceiveTimeout = 5000;

        ResponseData = Encoding.ASCII.GetBytes("SomeResponseData");

        ClientEP = new IPEndPoint(IPAddress.Any, 0);
        ClientRequestData = Server.Receive(ref ClientEP);
        ClientRequest = Encoding.ASCII.GetString(ClientRequestData);

        Server.Send(ResponseData, ResponseData.Length, ClientEP);

        ipv4 = ClientEP.Address.ToString();
        Debug.Log(ipv4);
        if (!string.IsNullOrEmpty(ipv4)) {
            Connect();
        }
    }

    void OnGUI() {
        string localipv4 = IPManager.GetIP(ADDRESSFAM.IPv4);
        GUI.Box(new Rect(10, Screen.height - 50, 100, 50), localipv4);
        GUI.Label(new Rect(20, Screen.height - 30, 100, 20), "Status:" + GlobalSettings.Client.isConnected);
    }
    
    void Start() {
        GlobalSettings.Client = new NetworkClient();
    }
 
    public void Connect() {
        GlobalSettings.Client.Connect(ipv4, 25001);
    }

    void Update() {

    }
}

