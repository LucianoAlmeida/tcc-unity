﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class VibrationManager : MonoBehaviour {

    void Awake() {
        GlobalSettings.Client.RegisterHandler(889, VibrateOnCollision);
    }

    private void VibrateOnCollision(NetworkMessage message) {
        StringMessage msg = new StringMessage();
        msg.value = message.ReadMessage<StringMessage>().value;

        string[] args = msg.value.Split('|');
        int duration = int.Parse(args[1]);
        Vibration.CreateOneShot(duration);
    }

    public void QuitButton() {
        Vibration.CreateOneShot(30, 1);
    }
    
    public void PauseButton() {
        Vibration.CreateOneShot(50, 10);
    }
        
}
