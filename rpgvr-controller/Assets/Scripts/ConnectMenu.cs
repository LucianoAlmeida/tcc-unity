using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class ConnectMenu : MonoBehaviour {

    public GameObject cm;
    public GameObject dm;

    public void ConnectToDevice(){
        cm.SetActive(false);
        dm.SetActive(true);
    }

    public void DisconnectFromDevice(){
        Debug.Log("Needs to implement disconnection function");
        cm.SetActive(true);
        dm.SetActive(false);
    }

    public void GoToController(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void GoToMenu(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
