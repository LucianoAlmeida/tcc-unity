using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;


public class JoystickScript : MonoBehaviour {
    public Image circle;
    public Image slider;
    public Text debugText;
    public Text xText;
    public Text yText;

    private Vector2 pointA;
    private Vector2 pointB;
    private Vector2 offset;
    private Vector2 direction;
    private Vector2 amount;
    private bool touchStart;
    private bool singleTouch;

    private Vector3 acceleration;
    public float magnitude;
    public float interval;
    private float lastShake;
    private bool isAttacking;


    void Start() {
        circle.enabled = false;
        slider.enabled = false;
        touchStart = false;
        isAttacking = false;
    }

    void Update() {
        if (!isAttacking) {
            DetectTouch();
        }
        DetectShake();
    }

    void FixedUpdate() {
        if (!isAttacking) {
            SendTouch();
        }
    }

    public void SendTouch() {
        if (singleTouch) {
            SendJoystickInfo("action", Vector2.zero);
            singleTouch = false;
        }
	if (touchStart) {
	    offset = pointB - pointA;
	    direction = Vector2.ClampMagnitude(offset, 1.0f);
	    amount = Vector2.ClampMagnitude(offset, 100.0f);
	    SendJoystickInfo("slide", direction);
	    slider.transform.position = new Vector2(pointA.x + amount.x, pointA.y + amount.y);
	} else {
            if (!Vector2.Equals(direction, Vector2.zero)) {
                direction = Vector2.zero;
                SendJoystickInfo("slide", direction);
                circle.enabled = false;
                slider.enabled = false;
            }
	}
    }
    
    public void DetectTouch() {
	if (Input.GetMouseButtonDown(0)) {
	    pointA = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
	    pointB = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

	    circle.transform.position = pointA;
	    slider.transform.position = pointA;
	}
	if (Input.GetMouseButtonUp(0)) {
            //must check if equal AND click was in clickable area
	    if(pointB == pointA) {
                singleTouch = true;
            }
        }
	if (Input.GetMouseButton(0)) {
	    pointB = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            float touchDistance = Vector2.Distance(pointA, pointB);

	    if(touchDistance > 0.2f) {
		touchStart = true;
		circle.enabled = true;
		slider.enabled = true;

		xText.text = "Xb=" + Mathf.Floor(pointB.x);
		yText.text = "Yb=" + Mathf.Floor(pointB.y);
		debugText.text = "Move";
	    } else {
		xText.text = "Xa=" + Mathf.Floor(pointA.x);
		yText.text = "Ya=" + Mathf.Floor(pointA.y);
		debugText.text = "Select";
	    }
	} else {
	    touchStart = false;
	}
    }

    public void DetectShake() {
        acceleration = Input.acceleration;
        if (Time.unscaledTime >= lastShake + interval) {
            isAttacking = false; 
            if (acceleration.sqrMagnitude >= magnitude) {
                isAttacking = true;
                debugText.text = "Attack";
                xText.text = "";
                yText.text = "";
                SendJoystickInfo("shake", Vector2.zero);
                lastShake = Time.unscaledTime;
            }
        }
    }

    public static void SendJoystickInfo(string command, Vector2 direction) {
        if (GlobalSettings.Client != null) {
            if (GlobalSettings.Client.isConnected) {
                StringMessage msg = new StringMessage();
                msg.value = command + "|" + direction.x + "|" + direction.y;
                GlobalSettings.Client.Send(888, msg);
            } else {
                Debug.Log("Client not connected. Input not sent");
            }
        } else {
            Debug.Log("Null client");
        }
    }
}
