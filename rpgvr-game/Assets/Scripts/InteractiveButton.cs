﻿using UnityEngine;
using UnityEngine.UI;

public class InteractiveButton : MonoBehaviour {
    private GameObject selected;
    private float hover = 2f;
    private float sightlenght = 50f;
    private Camera camera;
    public GameObject lvlloader;
    private AudioSource audioSource;
    public AudioClip clip;

    void Awake() {
        audioSource = GetComponent<AudioSource>();
    }

    void Start() {
        camera = Camera.main;
    }
    
    void FixedUpdate() {
        RaycastHit hit;
        Ray direction = new Ray(camera.transform.position, camera.transform.forward);

        if (Physics.Raycast(direction, out hit, sightlenght)) {
            Debug.DrawLine(direction.origin, hit.point);
            if (hit.collider.tag == "Button") {
                if (selected != null && selected != hit.transform.gameObject) {
                    GameObject newobj = hit.transform.gameObject;
                    audioSource.Play();
                    HighlightButton(newobj);
                }
                selected = hit.transform.gameObject;
            }
        }

    }

    void Update() {
        if (GlobalSettings.Command == "singletouch") {
            if (selected.name == "Start") {
                //Needs to add the connection to globalsettings
                //yet, somethings is not working in the loading screen
                GlobalSettings.Command = "";
                GlobalSettings.Position = Vector2.zero;
                lvlloader.GetComponent<LevelLoader>().LoadLevel(1);
            }
            if (selected.name == "Credits") {
                GlobalSettings.Command = "";
                Debug.Log("credits");
            }
        }
    }

    private void HighlightButton(GameObject newobj) {
        newobj.transform.GetChild(0).gameObject.SetActive(true);
        selected.transform.GetChild(0).gameObject.SetActive(false);
        selected = newobj;
    }
}
