﻿using UnityEngine;

public class CharacterStats : MonoBehaviour {

    public int maxHealth = 100;
    public int currentHealth { get; set; }
    private Animator animator;
    private bool dead;
    
    public Stat damage;
    public Stat Armor;
    
    void Awake() {
        animator = GetComponent<Animator>();
        currentHealth = maxHealth;
        dead = false;
    }

    public void TakeDamage(int damage) {
        if (!isDead()) {
            currentHealth -= damage;
            animator.SetTrigger("Hit");
        }
        if (currentHealth <= 0) {
            Die();
        }
    }

    public bool isDead(){
        return dead;
    }

    public virtual void Die() {
        if (!isDead()){
            animator.SetTrigger("Dead");
            Debug.Log(transform.name + " died!");
            dead = true;
        }
    }
}
