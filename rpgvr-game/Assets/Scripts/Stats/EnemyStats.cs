﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyStats : CharacterStats {
    public TextMesh hp;
    private GameObject player;
    private PlayerStats playerStats;
    public float radius;

    void Start() {
        player = PlayerManager.instance.player;
        playerStats = player.GetComponent<PlayerStats>();
    }

    void Attack() {
        float distance = Vector3.Distance(player.transform.position, transform.position);
        if (distance <= radius) {
            playerStats.TakeDamage(this.damage.GetValue());
        }
    }

}
