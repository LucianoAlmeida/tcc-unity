﻿using UnityEngine;
using System.Collections;

public class MoveCameraWithPlayer : MonoBehaviour {
    public Transform chest;
    public Transform rarm;
    public Transform cameraObj;
    private Transform camera;
    public float offxz;
    public float offy;

    private Vector3 cameraRot;
    private Vector3 playerRot;
    private Quaternion quat;

    private float x, z;
    public float xqtd, yqtd, zqtd;

    void Start() {
        camera = Camera.main.transform;
    }

    void Update() {
        cameraRot = camera.transform.rotation.eulerAngles;
        playerRot = transform.rotation.eulerAngles;
    }
    
    void LateUpdate() {
        x = offxz * Mathf.Sin(camera.rotation.eulerAngles.y * Mathf.Deg2Rad);
        z = offxz * Mathf.Cos(camera.rotation.eulerAngles.y * Mathf.Deg2Rad);
        cameraObj.transform.position = chest.position + new Vector3(x, offy, z);
        quat = camera.transform.rotation;
        quat.x = 0;
        quat.z = 0;
        transform.rotation = quat;
    }
}

// -------Rotate bones        
//    public float xqtd, yqtd, zqtd;
//        Vector3 temp = rarm.rotation.eulerAngles;
//        temp.x += xqtd;
//        temp.y += yqtd;
//        temp.z += zqtd;
//        rarm.rotation = Quaternion.Euler(temp);
