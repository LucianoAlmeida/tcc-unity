﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class MapsConnection: MonoBehaviour {

    private string ipv4;

    void OnGUI() {
        ipv4 = IPManager.GetIP(ADDRESSFAM.IPv4);
        GUI.Box(new Rect(10,Screen.height -50, 100, 50), ipv4);
        GUI.Label(new Rect(20,Screen.height -35, 100, 20), "Status:" + NetworkServer.active);
        if (GlobalSettings.Controller != null)
            GUI.Label(new Rect(20,Screen.height -20, 100, 20), "Connected:" + GlobalSettings.Controller.isConnected);
    }
}
