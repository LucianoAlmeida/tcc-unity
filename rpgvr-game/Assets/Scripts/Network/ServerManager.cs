﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Net;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;


public class ServerManager : MonoBehaviour {
    //Necessary to discover screen IP
    public UdpClient Client;
    public byte[] RequestData;
    public IPEndPoint ServerEP;
    byte[] ServerResponseData;
    public string ServerResponse;
    string ipv4;
    public Vector2 position;
    

    void OnGUI() {
        ipv4 = IPManager.GetIP(ADDRESSFAM.IPv4);
        GUI.Box(new Rect(10,Screen.height -50, 100, 50), ipv4);
        GUI.Label(new Rect(20,Screen.height -35, 100, 20), "Status:" + NetworkServer.active);
        if (GlobalSettings.Controller != null)
            GUI.Label(new Rect(20,Screen.height -20, 100, 20), "Connected:" + GlobalSettings.Controller.isConnected);
    }

    void Start() {
        Client = new UdpClient();
        Client.Client.SendTimeout = 5000;
        Client.Client.ReceiveTimeout = 5000;
        RequestData = Encoding.ASCII.GetBytes("SomeRequestData");

        ServerEP = new IPEndPoint(IPAddress.Any, 0);
        Client.EnableBroadcast = true;

        NetworkServer.Listen(25001);

        NetworkServer.RegisterHandler(888, ServerReceiveMessage);
    }

    void Update() {
        if (GlobalSettings.Controller == null) {
            Client.Send(RequestData, RequestData.Length, new IPEndPoint(IPAddress.Broadcast, 8888));
            //Debug.Log("enviando mensagem via broadcast");
            foreach (NetworkConnection nc in NetworkServer.connections) {
                if (nc != null) {
                    GlobalSettings.Controller = nc;
                }
            }
        } else {
//            Debug.Log(control.hostId);
//            Debug.Log(control.isConnected);
            
        }
    }

    private void ServerReceiveMessage(NetworkMessage message) {
        StringMessage msg = new StringMessage();
        msg.value = message.ReadMessage<StringMessage>().value;

        string[] deltas = msg.value.Split('|');
        string command = deltas[0];
        float xval = float.Parse(deltas[1], CultureInfo.InvariantCulture.NumberFormat);
        float yval = float.Parse(deltas[2], CultureInfo.InvariantCulture.NumberFormat);
        GlobalSettings.Command = command;
        GlobalSettings.Position = new Vector2(xval, yval);
    }
 }
