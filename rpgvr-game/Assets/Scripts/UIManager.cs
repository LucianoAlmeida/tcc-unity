﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour {
    private PlayerStats playerStats;
    private GameObject player;
    public GameObject camera;
    public GameObject GameOverObj;

    public GameObject panel;
    public TextMeshProUGUI msgText;

    void Start() {
        player = PlayerManager.instance.player;
        playerStats = player.GetComponent<PlayerStats>();
        panel = this.transform.GetChild(0).gameObject;
    }
    
    void Update() {
        if (playerStats.currentHealth > 75) {
            panel.SetActive(false);
        }
        if (playerStats.currentHealth <= 75 && playerStats.currentHealth > 50) {
            panel.SetActive(true);
            Image img = panel.GetComponent<Image>();
            img.color = new Color(1f, 1f, 0f, 0.2f);
        }
        if (playerStats.currentHealth <= 50 && playerStats.currentHealth > 25) {
            panel.SetActive(true);
            Image img = panel.GetComponent<Image>();
            img.color = new Color(1f, 0.5f, 0f, 0.2f);
        }
        if (playerStats.currentHealth <= 25 && playerStats.currentHealth > 0) {
            panel.SetActive(true);
            Image img = panel.GetComponent<Image>();
            img.color = new Color(1f, 0f, 0f, 0.2f);
        }
        if (playerStats.currentHealth <= 0) {
            GameOverScreen();
        }
    }

    void GameOverScreen() {
        GameOverFadePanel();
        FadeVolume();
    }

    void GameOverFadePanel() {
        panel.SetActive(true);
        Image img = panel.GetComponent<Image>();
        img.sprite = null;
        img.color = new Color(0f, 0f, 0f, Mathf.Lerp(img.color.a, 1f, Time.deltaTime * 2.5f));
        if (img.color.a >= 0.99f){
            camera.transform.position = new Vector3(100,100,100);
            this.GetComponent<Canvas>().planeDistance = 3f;
            player.GetComponent<MoveCameraWithPlayer>().enabled = false;
            GameOverObj.SetActive(true);
            if (GlobalSettings.Command == "singletouch"){
                GlobalSettings.Command = "";
                GlobalSettings.Position = Vector2.zero;
                AudioListener.volume = 1f;
                SceneManager.LoadScene(1);
            }
        }
    }

    public void HealPanel() {
        StartCoroutine(ShowMessage("HEALED", 1f));
    }

    IEnumerator ShowMessage (string message, float delay) {
        msgText.text = message;
        msgText.enabled = true;
        yield return new WaitForSeconds(delay);
        msgText.enabled = false;
    }

    void FadeVolume() {
        AudioListener.volume = Mathf.Lerp(AudioListener.volume, 0f, Time.deltaTime * 2.5f);
    }
}
