﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour {
        
    private Animator animator;
    private Camera camera;
    private EnemyStats enemyStats;
    private PlayerStats playerStats;
    private GameObject enemy;

    private float horizontalInput;
    private float verticalInput;
    private bool attack;
    private bool selectAction;
    public float attackDistance;

    public GameObject fireball;
    public float projectileSpeed;
    public Vector3 projectileOffset;

    public GameObject canvas;

    void Start() {
        playerStats = this.GetComponent<PlayerStats>();
        animator = GetComponent<Animator>();
        camera = Camera.main;
        attack = false;
        selectAction = false;
    }
    
    void Update() {
        PlayerMovement();
    }

    void PlayerMovement() {
        #if UNITY_ANDROID && !UNITY_EDITOR
            if (GlobalSettings.Command == "singletouch") {
                GlobalSettings.Command = "";
                SelectAction(true);
            }
            if (GlobalSettings.Command == "slide") {
                MoveAction(true);
            }
            if (GlobalSettings.Command == "shake") {
                GlobalSettings.Command = "";
                AttackAction(true);
            }
        #endif
        #if UNITY_EDITOR
            SelectAction(false);
            MoveAction(false);
            AttackAction(false);
        #endif
    }

    void SelectAction(bool android) {
        if (android){
            selectAction = true;
        } else {
            if (Input.GetMouseButtonDown(1)) {
                selectAction = true;
            }
        }

        if (selectAction) {
            selectAction = false;

            RaycastHit hit;
            Ray direction = new Ray(camera.transform.position, camera.transform.forward);

            if (Physics.Raycast(direction, out hit, 3f)) {
                if (hit.collider.tag == "Heal") {
                    PlayerManager.instance.player.GetComponent<PlayerStats>().currentHealth = 100;
                    canvas.GetComponent<UIManager>().HealPanel();
                }
            }
        }
    }

    void MoveAction(bool android) {
        if (android) {
            horizontalInput = GlobalSettings.Position.x;
            verticalInput = GlobalSettings.Position.y;
        } else {
            horizontalInput = Input.GetAxis("Horizontal");
            verticalInput = Input.GetAxis("Vertical");
        }

        if (horizontalInput != 0 || verticalInput != 0) {
            animator.SetBool("Moving", true);
            animator.SetFloat("Direction", horizontalInput);
            animator.SetFloat("Speed", verticalInput);
        } else {
            animator.SetBool("Moving", false);
        }
    }

    void AttackAction(bool android) {
        if (android) {
            attack = true;
        } else {
            if(Input.GetMouseButtonDown(0)) {
                attack = true;
            }
        }

        if (attack) {
            attack = false;
            animator.SetTrigger("Attack");
        }
    }

    void AttackAnimation() {
        GameObject fireballInstance;
        fireballInstance = Instantiate(fireball, camera.transform.position - projectileOffset, camera.transform.rotation) as GameObject;

        Rigidbody rigidbody;
        rigidbody = fireballInstance.GetComponent<Rigidbody>();
        rigidbody.AddForce(camera.transform.forward * projectileSpeed, ForceMode.VelocityChange);
        Destroy(fireballInstance, 0.6f);
    }
}
