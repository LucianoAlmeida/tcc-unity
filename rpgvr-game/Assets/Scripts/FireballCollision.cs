﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballCollision : MonoBehaviour {
    private EnemyStats enemyStats;
    private PlayerStats playerStats;

    void Start() {
        playerStats = PlayerManager.instance.player.GetComponent<PlayerStats>();
    }
    
    void OnCollisionEnter(Collision col) {
        if (col.collider.tag == "Enemy") {
            enemyStats = col.gameObject.GetComponentInChildren<EnemyStats>();
            enemyStats.TakeDamage(playerStats.damage.GetValue());
        }
        Destroy(this.gameObject);
    }
}
