﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {
    public float lookRadius = 10f;
    public Animator animator;
    private Transform target;
    private NavMeshAgent agent;
    private float moveSpeed;
    private bool isAttacking;
    private bool isIdle;
    private EnemyStats stats;

    void Start() {
        agent = GetComponent<NavMeshAgent>();
        target = PlayerManager.instance.player.transform;
        isAttacking = false;
        isIdle = false;
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    void Update() {
        stats = this.GetComponentInChildren<EnemyStats>();
        if (!stats.isDead()) {
        //if (!dead) {
            float distance = Vector3.Distance(target.position, transform.position);
            if (distance > lookRadius && !isIdle){
                agent.enabled = false;
                isIdle = true;
            }

            if (distance <= lookRadius && !isAttacking) {
                agent.enabled = true;
                if (distance <= agent.stoppingDistance) {
                    moveSpeed = 0f;
                    animator.SetFloat("speedv", moveSpeed);
                    isAttacking = true;
                    animator.SetTrigger("Attack");
                } else {
                    moveSpeed = 2f;
                    animator.SetFloat("speedv", moveSpeed);
                    agent.SetDestination(target.position);
                    isIdle = false;
                }
            } else {
                FaceTarget();
                distance = 0f;
                animator.SetFloat("speedv", distance);
            }

            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Attack")) {
                isAttacking = false;
            }
        } else {
            agent.enabled = false;
            this.GetComponent<CapsuleCollider>().enabled = false;
        }
    }

    void FaceTarget() {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5);
    }
}
