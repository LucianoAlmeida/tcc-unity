﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour {
    public GameObject loadingScreen;
    public GameObject info;
    public Slider slider;
    public Text progressText;
    private Image img;

    public void LoadLevel (int sceneIndex) {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator FadeIn() {
        for (float i = 0; i <= 1; i += Time.deltaTime) {
            img.color = new Color(1, 1, 1, i);
            yield return null;
        }
    }

    IEnumerator LoadAsynchronously (int sceneIndex) {
        loadingScreen.SetActive(true);
        img = loadingScreen.GetComponent<Image>();
        for (float i = 0; i <= 1f; i += Time.deltaTime) {
            img.color = new Color(0, 0, 0, i);
        }
        img.color = new Color(0, 0, 0, 1);
        info.SetActive(true);

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone) {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            slider.value = progress;
            progressText.text = progress * 100f + "%";
            yield return null;
        }
    }
}
