﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public static class GlobalSettings {
    public static NetworkConnection Controller {get;set;}
    public static Vector2 Position {get;set;}
    public static string Command {get;set;}

    public static void SendMessage(string type, int duration) {
        if (GlobalSettings.Controller != null) {
            if (GlobalSettings.Controller.isConnected) {
                StringMessage msg = new StringMessage();
                msg.value = type + "|" + duration;
                GlobalSettings.Controller.Send(889, msg);
            } else {
                Debug.Log("Controller not connected. Input not sent");
            }
        } else {
            Debug.Log("Null controller. Tried to send message, but you are not connected!");
        }
    }
}
