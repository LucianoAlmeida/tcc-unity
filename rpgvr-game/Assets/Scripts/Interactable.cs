﻿using UnityEngine;

public class Interactable : MonoBehaviour {
    public float radius;
    private bool isFocus = false;
    Transform player;

    public virtual void Interact() {
        //Virtual method to be overwritten by the different objects
        Debug.Log("Interact");
    }

    void Update() {
        if (isFocus) {
            float distance = Vector3.Distance(player.position, transform.position);
            if (distance <= radius) {
                Interact();
            }
        }
    }
    
    public void OnFocused(Transform playerTransform) {
        isFocus = true;
        player = playerTransform;
    }

    public void OnDefocused() {
        isFocus = false;
        player = null;
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
