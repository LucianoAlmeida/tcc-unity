﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class SoundController : MonoBehaviour {
    private AudioSource audioSource;
    private Animator anim;
    public AudioClip[] audioClips;

    private const int _IDLE = 0;
    private const int _STEP = 1;
    private const int _ATTACK = 2;
    private const int _HIT  = 3;
    private const int _DEAD = 4;
    private const int _VOICE = 5;
    
    void Start() {
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }
    
    void PlayIdle() {
        float rng = Random.Range(0f, 10f);
        if (rng >= 3f) {
            audioSource.PlayOneShot(audioClips[_IDLE]);
        }
    }

    void PlayStep() {
        audioSource.pitch = Random.Range(0.8f, 1.1f);
        audioSource.volume = Random.Range(0.4f, 0.6f);
                                         
        audioSource.PlayOneShot(audioClips[_STEP]);
    }

    void PlayAttack() {
        audioSource.PlayOneShot(audioClips[_ATTACK]);
    }

    void PlayHit() {
        audioSource.PlayOneShot(audioClips[_HIT]);
    }

    void PlayDead() {
        audioSource.PlayOneShot(audioClips[_DEAD]);
    }

    void PlayVoice() {
        audioSource.PlayOneShot(audioClips[_VOICE]);
    }

    void WalkForward() {
        float speed = anim.GetFloat("Speed");
        float direction = anim.GetFloat("Direction");
        if (Mathf.Abs(speed) > Mathf.Abs(direction))
            PlayStep();
    }

    void WalkSideway() {
        float speed = anim.GetFloat("Speed");
        float direction = anim.GetFloat("Direction");
        if (Mathf.Abs(speed) <= Mathf.Abs(direction))
            PlayStep();
    }

    void SendVibration(int time) {
        GlobalSettings.SendMessage("collision", time);
    }
}
